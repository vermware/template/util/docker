#!/bin/bash

set -e

# cd to script dir, fix relative paths
cd "$(dirname "${BASH_SOURCE[0]}")"

# config
readonly TARGETS_DIR="../docker_targets"

# defaults
unset BUILD
unset LIST
unset PUSH
unset TARGET

# constants
readonly CMD="$0"

# functions
function print_help()
{
	printf '%s\n' "\
$CMD [OPTION]... TARGET

	-b, --build
		build specified target

	-h, --help
		print this help and exit

	-l, --list
		list available targets and terminate
		they should be placed in $TARGETS_DIR
		e.g. $TARGETS_DIR/debian_testing/Dockerfile
			regular Dockerfile
		e.g. $TARGETS_DIR/debian_testing/Tagfile
			one-liner containing tag to be used

	-p, --push
		push specified target to registry"

	return 0
}

# print usage if no options
if [[ $# == 0 ]]
then
	print_help
	exit 1
fi

# parse options
while (( $# > 0 ))
do
	case "$1" in
	-b|--build)
		BUILD=true
		;;
	-h|--help)
		print_help
		exit 0
		;;
	-l|--list)
		LIST=true
		;;
	-p|--push)
		PUSH=true
		;;
	*)
		# target only allowed as final option
		[[ $# != 1 ]] && printf "invalid options\n" && exit 1
		TARGET="$TARGETS_DIR/$1"
		;;
	esac

	shift
done

# list
if [[ $LIST ]]
then
	# loop over filelist
	for dir in "$TARGETS_DIR/"*
	do
		# skip if not directory
		[[ ! -d $dir ]] && continue

		# print target
		printf "$(basename "$dir")\n"

		# warn if no Dockerfile
		[[ ! -f $dir/Dockerfile ]] && printf "\t(warn: no Dockerfile)\n"

		# warn if no Tagfile
		[[ ! -f $dir/Tagfile ]] && printf "\t(warn: no Tagfile)\n"
	done

	# terminate after listing
	exit 0
fi

# verify target
[[ ! -d $TARGET ]] && printf "invalid target\n" && exit 1
[[ ! -f $TARGET/Dockerfile ]] && printf "no Dockerfile\n" && exit 1
[[ ! -f $TARGET/Tagfile ]] && printf "no Tagfile\n" && exit 1

# cd to target
cd "$TARGET"

# read and verify tag
TAG="$(< Tagfile)"
(( ${#TAG} < 1 )) && printf "invalid Tagfile\n" && exit 1

# build
[[ $BUILD ]] && docker build --pull -t "$TAG" .

# push
[[ $PUSH ]] && docker push "$TAG"

exit 0

#!/bin/bash

set -e

# fake the docker command
export PATH="$PWD/path:$PATH"

printf "TEST: LIST TARGETS\n"
./docker/image.sh -l
printf "\n"

printf "TEST: BUILD TARGET\n"
./docker/image.sh -b valid_target
printf "\n"

printf "TEST: PUSH TARGET\n"
./docker/image.sh -p valid_target
printf "\n"

printf "TEST: PUSH TARGET WITH WHITESPACE\n"
./docker/image.sh --push "whitespace BAD but VALID"

exit 0

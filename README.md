# CI CD / Docker

Various helper scripts to ease workflow with Docker and to help with automated
deployment of Docker-related things. Currently just one.

## image.sh

Helper script that automates building and pushing of docker images.
You would usually place this repo, as a submodule, into `ci/docker`.
The targets would then be stored under `ci/docker_targets`.

```
./image.sh [OPTION]... [TARGET]

        -b, --build
                build specified target

        -h, --help
                print this help and exit

        -l, --list
                list available targets and terminate
                they should be placed in ../docker_targets
                e.g. ../docker_targets/debian_testing/Dockerfile
                        regular Dockerfile
                e.g. ../docker_targets/debian_testing/Tagfile
                        one-liner containing tag to be used

        -p, --push
                push specified target to registry
```
